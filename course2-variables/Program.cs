﻿using System; // nereste

namespace course2variables // nereste
{
	class MainClass // nereste
	{
		// nase stara funkce Main()

		public static void Main (string[] args)
		{
			// vytvorili jsme svoji prvni promennou
			// promennou si predstavte jako misto v pameti, ktere obsahuje nejakou hodnotu (treba cislo) a to misto si nejak pojmenuje, tady treba i
			int i;

			// takze mame deklarovanou promennou i, a protoze sme pred ni napsali slovo int, bude moct obsahovat cela cisla (integer)
			// zatim sme do i zadnou hodnotu neulozili, takze ma defaultni hodnotu, kterou ji priradi runtime engine (.net framework)
			// defaultni hodnota pro int(eger) je 0

			//i = 0; // do promenne ulozime hodnotu 0, tim se stane ze v pameti programu v bunce, ktera se jmenuje i, se zapise hodnota 0

			// o tom ze tam je ulozena 0 se presvedcime tak, ze si obsah promenne vypiseme
			// ale nevime jak? vime! udelame to stejne, jako sme psali Hello world, akorat ze misto textu tam hodime jmeno promenne i
			Console.WriteLine (i); // vsimnete si ze na rozdil od Hello world, i neni v uvozovkach, to je dulezite!!! proc?

			// spustte to!!!
			// po spusteni uvidite ze vam prekladac nadava, ze blaboli neco o pouziti promenne ktere nebyla prirazena hodnota
			// a vite co? prekladac ma pravdu... z bezpecnostnich duvodu se nedovoluje cteni z promenne, do ktere sme nic nezapsali,
			// a to i navzdory faktu, ze ji asi vynuloval runtime (toto slovo vam vysvetlim)

			// takze, co s tim udelame?
			// odkomentujte radek cislo 19 a uvidite ze program uz muzete spustit a vypise se vam 0

			// no ale protoze nula od nuly pojde a ikdyz to je zlate cislo, samotna nam je nanic

			// s promennyma muzete delat vselijake operace, ktere se co se zapisu tyce dost podobaji klasickym vzorcum
			// my si tedkom deklarujeme jeste jednu promennou a rovnou ji priradime pocatecni hodnotu... to udelame takto

			int j = 1; // deklarujeme promennou j a dame ji hned hodnotu 1

			// a jeste si deklarujeme promennou k
			int k;

			// mozna premyslite proc pojmenovavam promenny i,j,k ... je to takova konvecne pokud pisete kratkej kod a nechce se vam vymyslet nejaky genialni nazvy promennych,
			// tak proste jedete podle abecedy, ale protoze nase prvni promenna i vsechny ostatni jsou typu integer, tak zaciname na abecede od pismena i(nteger)

			// ted se drzte, napiseme svuj prvni vyraz - technicky je to sice prikaz, ale dost se podoba matematickemu vzorci - vyrazu
			k = i + j; // co bude po spusteni tohodle prikazu v promenne k? doufam, ze je to jasne, ale pro jistotu si to vypiseme

			Console.WriteLine (k);

			// pokud by ste si takto chteli jen vypsat soucet promennych, teoreticky nemusite pouzit vubec promennou k, jde to i takto, ze 
			// vyraz primo napisete jako parametr WriteLine

			Console.WriteLine (i + j);
		}
	}
}
