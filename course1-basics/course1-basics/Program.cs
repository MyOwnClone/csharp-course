﻿// toto je komentar, sem si muzes psat, co chces, prekladac to neresi
// komentar se vytvori tak, ze napises // a pak si za nej pises co chces

/* tohle je viceradkovy komentar, abys nemusel psat na zacatku kazdeho radku v dlouhem komentari // na prvni  
 radek viceradkoveho komentare napises /* a na posledni */

/*

na formatovani u komentaru az tak nezalezi, prvni radek v komentari muzete nechat krome uvozeni komentare prazdny,
a posledni taky

*/

using System; // dava vedet prekladaci (to si vysvetlime co je, ze chceme pracovat s knihovnou system)

/*
 * knihovna - soubor trid a funkci, musis ji pouzit pokud chces delat neco co saha mimo tvuj kod, treba na OS
 * knihovna System zpristupnuje zakladni tridy a funkce pro praci se systemem, prave Console.WriteLine() nize
 */

// namespaces zatim nereste, berte to jako ze to tu musi byt, vysvetleno pozdeji
namespace course1basics
{
	// class tez nereste, berte to jako ze to tu musi byt, vysvetleno pozdeji
	class MainClass
	{
		// tady se dostavame k necemu zajimavejsimu

		// Main() je funkce, resp metoda, technicky funkce je funkce ktera nenalezi k zadnemu objektu, metoda je funkce nalezejici k objektu
		// slova public static zatim nereste
		// co je to vlastne funkce? jaky ma smysl psat funkce? pri programovani casto rozkladame problem ktery resime na mensi podukoly 
		// a ty prvne resime a pak to reseni skladame z volani vic metod, je to metoda "zdola nahoru", kdy program jakoby stavime z cihel-
		// to jsou funkce a volani mezi nimi je malta ktera je drzi pohromade

		// no a prave toto je nase prvni funkce, jmenuje se Main
		// v C# se standartne pojmenovavaji funkce s prvnim pismenem velkym, neni to nutne, ale vetsina vyvojaru to dodrzuje jako stabni kulturu
		// rikal sem ze slova public a static zatim nereste, ale co znamena slovo void?
		// slovo void primo souvisi s nazvem "funkce", kdyz si vzpomenete na matiku, funkce ma parametr a dava nam nejakou hodnotu
		// tohle je presne ono, az na to ze tato funkce nam zadnou hodnotu nevraci, takze tam proste napiseme void (coz znamena prazdny)
		// jak uz sem psal, vetsina funkci ma nejake parametry, tady jsou parametry to, co je v zavorkach - string[] args, zatim to nereste
		// a posledni dulezita vec je, co je v tele funkce, sem davame prikazy co chceme aby program provadel
		// funkce Main je trosku specialni, tim, ze pokud spustite vasi aplikaci (.exe na windows32/64), operacni system prvne spusti funkci Main()
		// takze funkci main se vlastne spousti vas program a z ni musite volat ostatni funkce ve vasem kodu
		public static void Main (string[] args)
		{
			// nas prvni prikaz, to ze se sklada ze dvou slov Console a WriteLine zatim nereste...
			// proste to zatim chapejte tak, ze Console musi byt napsano pred WriteLine
			// jak vidite, volame zde funkci a rovnou ji pri volani davame parametry (to v zavorkach () )
			// funkce WriteLine se chova tak, ze napise na konzoli (prikazovou radku) to, co ji vy date jako parametr
			// pokud chcete pouzivat v programu text, musite ho zapsat do uvozovek, jinak to prekladac nepochopi
			// a bude se to snazit chapat jako prikazy C#
			// takze si program pomoci ctrl+f5 na windows nebo option-cmd-enter na Macu spustte!
			Console.WriteLine ("Hello World!");

			// jak vidite, funkce se jmenuje WriteLine(), Write je logicke (je to zapis), ale proc je za tim to Line?
			// Line tam znamena ze toto volani vam po vypsani vaseho textu v konzoli jeste odradkuje
			// cili, existuje i funkce Write(), ktera naopak pouze vypise text, ale neodradkuje

			// jeste dve veci
			// C# patri do takzvane rodiny case sensitive jazyku, to znamena ze rozlisuje mezi velkyma a malyma pismenama
			// to znamena, ze pokud by ste volali predchozi funkci jako writeline, prekladac hodi chybu, protoze takova
			// funkce pro nej neexistuje, nevidi ji

			// a posledni vec, vidite ze WriteLine je funkce... a podobne jako nase funkce Main, nikdo nikde necte hodnotu kterou vraci
			// takze je WriteLine pravdepodobne stejne void jako nase funkce Main()
		}
	}

	// ukoly na hrani:
	// 1. zkuste zmenit text ktery se ma vypisovat
	// 2. zkuste oddelat strednik (;) za prikazem WriteLine, co se stane? Proc se to stane vam vysvetlim pozdeji.
	// 3. zkuste nasi funkci prejmenovat z Main na neco jineho, co se stane?
}
